#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Char.h>

// int state = 0;

// void KeyboardCallback(const std_msgs::CharConstPtr& msg)
// {
//   if (msg->data == 't'){
//     state = 1;
//   }else if(msg->data == 'y'){
//     state = 0;
//   }
// }

ros::Publisher point2_pub_left;
ros::Publisher point2_pub_right;

void PointCallback_right(const sensor_msgs::PointCloud2ConstPtr& msg)
{
  sensor_msgs::PointCloud2 output;
  output = *msg;
  output.header.frame_id = "/base_line_opposite";
  point2_pub_right.publish(output);
}

void PointCallback_left(const sensor_msgs::PointCloud2ConstPtr& msg)
{
  sensor_msgs::PointCloud2 output;
  output = *msg;
  output.header.frame_id = "/base_link";
  point2_pub_left.publish(output);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "my_tf_broadcaster");
  ros::NodeHandle node;

  tf::TransformBroadcaster br;
  tf::Transform transform;


  // ros::Subscriber sub = node.subscribe("/Keyboard_Input", 1000, KeyboardCallback);
  ros::Subscriber sub_1 = node.subscribe("/guidance/right/points2", 1000, PointCallback_right);
  point2_pub_right = node.advertise<sensor_msgs::PointCloud2>("Point2_published_right", 1);

  ros::Subscriber sub_2 = node.subscribe("/guidance/left/points2", 1000, PointCallback_left);
  point2_pub_left = node.advertise<sensor_msgs::PointCloud2>("Point2_published_left", 1);



  ros::Rate rate(10.0);
  while (node.ok()){

    transform.setOrigin( tf::Vector3(0,0,0) );
    transform.setRotation( tf::Quaternion(1.57, 0, 0, 0) );
    printf("transforming!\n");

    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "base_line_opposite"));










    rate.sleep();
    ros::spinOnce();
  }
  return 0;
};